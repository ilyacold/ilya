//
//  ViewController.swift
//  666
//
//  Created by iD Student on 7/9/15.
//  Copyright (c) 2015 1+1. All rights reserved.
//

import UIKit



class ViewController: UIViewController {
    
    var x:Double = 0
    var y:Double = 0
    
    var enterFlag = 1
    var yFlag = 1
    
    var decimalPoint = 0
    
    var power = 1
    
    var operationActive = 0
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label2: UILabel!

    @IBOutlet weak var displayedNumber: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func type(sender: AnyObject)
    {
        
        self.resignFirstResponder()
        
        label1.text = textField.text
        
    }
        
    @IBAction func digitals(sender: UIButton)
        
    {
        
       if enterFlag == 1
        
       {
        
        x = 0
        
        enterFlag = 0
        
        }
        
        if decimalPoint == 0
            
        {
        
            x = x * 10 + Double(sender.tag)
            
            switch "\(x)"
                
            {
                
            case let z where z.hasSuffix(".0"):
                
                self.displayedNumber.text = " " + String(Int(x))
                
            default: self.displayedNumber.text = "\(x)"
                
            }
            
         
            
        }
            
        else
            
        {
            
            x = x + Double(sender.tag)/pow(10, Double(power))
    
            power++
            
            switch "\(x)"
            {
                
            case let z where z.hasSuffix(".0"):
                
                self.displayedNumber.text = " " + String(Int(x))
                
            default: self.displayedNumber.text = "\(x)"
                
            }

        }
    }
    
    @IBAction func inverse(sender: AnyObject)
        
    {
        
        x = -x
        
        switch "\(x)"
        {
        case let z where z.hasSuffix(".0"):
            
            self.displayedNumber.text = "\(Int64(x))"
            
        default: self.displayedNumber.text = "\(x)"
        }

        
    }

    @IBAction func operations(sender: UIButton)
        
    {
        
        if enterFlag != 1 && yFlag == 1{
        
            
                switch operationActive
                    {
                    
                    case 10001:
                        x = y + x
                    case 10002:
                        x = y - x
                    case 10003:
                        x = y * x
                    case 10004:
                        x = y / x
                    default: break
                    
                     }
        
                }
        
        operationActive = sender.tag
        
        y = x
        
        yFlag = 1
        
        enterFlag = 1
        
        switch "\(x)"
        {
            
        case let z where z.hasSuffix(".0"):
            
            self.displayedNumber.text = " " + String(Int(x))
            
        default: self.displayedNumber.text = "\(x)"
            
        }

        
        decimalPoint = 0
        
        power = 1
    }
    
    @IBAction func decimal(sender: AnyObject)
    {
        if decimalPoint == 0
            
        {
            
        decimalPoint = 1
            
        self.displayedNumber.text = self.displayedNumber.text! + "."
            
        }
    }
    
    @IBAction func clearAllclear(sender: AnyObject)
    {
        
        decimalPoint = 0

        x = 0
        
        y = 0
        
        switch "\(x)"
        {
            
        case let z where z.hasSuffix(".0"):
            
            self.displayedNumber.text = " " + String(Int(x))
            
        default: self.displayedNumber.text = "\(x)"
            
        }

        
        enterFlag = 1
        
        yFlag = 1
        
        decimalPoint = 0
        
        power = 1
        
        operationActive = 0
    
    }

    
}

